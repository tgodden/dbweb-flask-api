# Example Web API
*Note: This code demonstrates how to use Flask for the purpose of creating a simple HTTP web API. For the project,
you should write your own code and use what you learn here!
Also take in mind that this code does **not** use a database, while you definitely should!*

## Why an API?
An Application Programming Interface (API) is the medium via which two online applications can communicate.
It acts as a bridge between a server application and a client application.

![Diagram of a client-server architecture with API](resources/APIDiagram.pdf)

By defining the interface used to access the server, developers of both the server and client application become more independent.
The client-side developers can create their application based on this API, even if the server application is not yet running.
Based on API specifications, the server-side developers know exactly what to implement and open up to the outside world.

APIs also make it easier to create multiple client-side applications.
For example, a webpage, smartphone application, desktop application and smart fridge application can each use the same API.
This means that the server-sided application does not have to change to accommodate these new client applications and their
potentially proprietary protocols.

Service providers often provide APIs for third parties, allowing independent developers easy access to the data.
This means that a third party can create applications that otherwise would need to be created by the service provider,
saving valuable time, effort and money.

## API Documentation
An API should always be well documented, so that the client knows which API calls to make, what arguments to pass and what
results to expect.
Examples of well-known APIs can easily be found on the internet. 

[Example: Twitter API Documentation](https://developer.twitter.com/en/docs/twitter-api/v1)

[Example: MIVB API Documentation](https://opendata.stib-mivb.be/store/data)

## Working with Flask
### Flask
Flask is a Python library that can be used to create and host simple web servers.
In this case, we will use Flask in order to create an API for our application.

To start off, we have to create a Flask object, which we will call `app`.
```python
app = flask.Flask(__name__)
```
In order to start the web server, we call:
```python
app.run()
```

### Flask API
In order to have different directories in our site, we need to attach Resources to the server.
We need to create a subclass of Resources for each directory we want.
Let's take a look at the Resource we implemented to get a list of all shows:
```python
# Get resource for ALL shows
class ShowsR(Resource):
    def get(self):
        return {"shows": [show.to_json() for show in shows.values()]}
```
In this case, we create a `ShowsR` subclass of `Resource`.
We define the `get` method, which is the method that will be executed when we perform a GET HTTP request.
Similarly, we could create `post` and `put` methods to support these HTTP request too.
If we return a Python Dict, it will automatically be converted to a JSON Object and returned to the requesting user.

To add the resource to the site, we simply use:
```python
api.add_resource(ShowsR, "/shows")
```
This means that when a client performs a GET request to *site.address/shows* (in our application <localhost:8080/shows>),
the ShowsR.get method is executed and the JSON value is returned to the client.

### Variables in URLs
It is also possible to have variables in URLs. In our application, <localhost:8080/shows/1> will return data from the show
with ID 1.
In this case, the variable is 1.

To support variables, we can use the following syntax when adding resources:
```python
api.add_resource(ShowR, "/shows/<int:show_id>")
```
ShowR is the Resource we use for displaying a single show.
In this code we define the variable as `<int:show_id>`. If we then add an extra argument with the name `show_id` to the
`get` or `post` method of ShowR, the value of this variable will be passed, and we will be able to use it when handling these methods:
```python
# Get resource for ONE show
class ShowR(Resource):
    def get(self, show_id):
        # Get show by id
        res = shows.get(show_id)
        # Show found, return information
        if res:
            return res.to_json()
        # Show not found
        else:
            return "Show not found", 404
``` 

### Request Data
As we know, data can also be passed in a request. In a GET request, this can be in a HTTP header; in a POST request this
can be either as HTTP header, or as a form, or both.

To get header data from the request in a Resource, we can use:
```python
flask.request.header
```
Similarly, to get form data from a POST request, we use:
```python
flask.request.form
```
How this data is structured depends on the client, so it is important that the API is defined clearly and correctly.

## Optional: Testing the code in Python
Although manual testing is possible, it is preferable to automate the testing.
This means that we don't always have to fill in a HTML form to try out our POST request every time we make a change.
For this example we will use the *unittest* library, although there are plenty other libraries that could be used.
We will not go over unittest in this course, especially since all information is readily available with a quick Google search.
What we will be discussing is how to make requests to our API from withing Python.

For this, we will use Python's *requests* library.
In order to GET all of our shows in Python, we can call:
```python
requests.get('localhost:8080/shows')
```
Similarly, in order to perform a POST request, we use
```python
requests.post('localhost:8080/shows')
```
Importanly, optional arguments can be supplied. The `header` and `data` arguments represent respectively the HTTP header
and the POST Form. 
It is possible to simply pass Python Dicts as these arguments, which will be converted to JSON Objects.

The result values of the `requests.get` and `requests.post` values are simply the values your browser would receive when
performing these requests.
This means that if, for example, a JSON Object is returned, you can test whether or not this JSON Object is what you expect it
to be, with libraries like *unittest*. 

