import unittest, requests, json

IP = '127.0.0.1'

class test(unittest.TestCase):
    # Test /shows
    def testShows(self):
        # Request all shows
        r = requests.get('http://127.0.0.1:8080/shows')
        # Check status code
        self.assertTrue(r.ok)
        # Convert result to json
        data = r.json()
        # Check whether data is correct
        titles = {s['title'] for s in data['shows']}
        self.assertEqual(titles, {'Amon Amarth', 'Aether Realm', 'Dark Tranquillity', 'Alestorm'})

    def testShow(self):
        # Request show data
        r = requests.get('http://127.0.0.1:8080/shows/1')
        # Check status code
        self.assertTrue(r.ok)
        # Convert result to json
        data = r.json()
        # Check whether data is correct
        self.assertEqual(data['title'], 'Dark Tranquillity')

    def testBuy(self):
        # Post with username and password
        data = {'user': 'aUser', 'password': 'theirPassword'}
        r = requests.post('http://127.0.0.1:8080/ticket/1', data=data)
        # Check status code
        self.assertTrue(r.ok)
