import flask
from flask_restful import Resource, Api

# Generate some id's for our shows
nextid = 0

# The Show class to store the data of our show.
class Show():
    def __init__(self, title, number_of_tickets):
        global nextid
        self.title = title
        self.number_of_tickets = number_of_tickets
        self.id = nextid
        nextid = nextid + 1

    # Convert the Show class to json
    def to_json(self):
        return {"title": self.title,
                "number_of_tickets": self.number_of_tickets,
                "id": self.id}


# Create a Show "database" called shows
showList = [Show("Amon Amarth", 5), Show("Dark Tranquillity", 2), Show("Aether Realm", 0), Show("Alestorm", 3)]
shows = {show.id: show for show in showList}


# === RESOURCES ===
# Get resource for ALL shows
class ShowsR(Resource):
    def get(self):
        return {"shows": [show.to_json() for show in shows.values()]}


# Get resource for ONE show
class ShowR(Resource):
    def get(self, show_id):
        # Get show by id
        res = shows.get(show_id)
        # Show found, return information
        if res:
            return res.to_json()
        # Show not found
        else:
            return "Show not found", 404


# Post resource for tickets
class TicketR(Resource):
    def post(self, show_id):
        # Get the passed data
        data = flask.request.form
        # Check whether user and password were passed
        if not ('user' in data and 'password' in data):
            return "UNAUTHORIZED", 401
        else:
            user = data['user']
        # Try to find show
        show = shows.get(show_id)
        # Show not found
        if not show:
            return "Show not found", 404
        # Out of tickets
        elif show.number_of_tickets <= 0:
            return "No more tickets available", 200
        # Good to go
        else:
            show.number_of_tickets = show.number_of_tickets - 1
            return "Ticket bought for user " + str(user), 200 


# Create our app
app = flask.Flask(__name__)
# Create an API for our app
api = Api(app)

# Add the resources to the API and specify the path
api.add_resource(ShowsR, "/shows")
api.add_resource(ShowR, "/shows/<int:show_id>")
api.add_resource(TicketR, "/ticket/<int:show_id>")

# Start the application
if __name__ == '__main__':
    app.run(port=8080)
